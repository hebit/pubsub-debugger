require("dotenv").config();

const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const Redis = require("ioredis");

const channel = process.env.REDIS_CHANNEL;

const io = new Server(server, {
  transports: ["websocket"],
});

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

io.on("connection", (socket) => {
  const redis = new Redis({
    port: process.env.REDIS_PORT, // Redis port
    host: process.env.REDIS_HOST, // Redis host
    tls: true,
    password: process.env.REDIS_PASS,
    db: 0,
  });

  let lastQuotation = null;

  redis.on("message", (channel, message) => {
    const product = JSON.parse(message);

    let diff = 0;
    if (lastQuotation) {
      diff =
        (new Date(product.lastUpdate).getTime() -
          new Date(lastQuotation.lastUpdate).getTime()) /
        1000;
    }

    socket.emit("new-quotation", { ...product, diff });
    lastQuotation = product;
  });

  redis.subscribe(channel, (err, count) => {
    if (err) console.error(err.message);
    console.log(`Subscribed to ${count} channels.`);
  });
});

server.listen(3000, () => {
  console.log("listening on *:3000");
});
